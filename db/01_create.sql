/* This is just a sample table */
CREATE TABLE settings (
	id SERIAL PRIMARY KEY,
	category VARCHAR(50) NULL,
	name VARCHAR(50) NOT NULL,
	value VARCHAR(5000) NOT NULL
);

/* Performance Runner */

/*
drop table perf_taskextra;
drop table perf_taskrun;
drop table perf_runner;
drop table perf_report;
 */

CREATE TABLE perf_report (
  id SERIAL PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  start TIMESTAMP NULL,
  stop TIMESTAMP NULL
);
CREATE INDEX idx_perf_report ON perf_report(name);

CREATE TABLE perf_runner (
  id SERIAL PRIMARY KEY,
  name VARCHAR(500) NOT NULL,
  description VARCHAR(1000) NULL,
  version VARCHAR(200) NULL
);
CREATE INDEX idx_perf_runner ON perf_runner(name, version);


CREATE TABLE perf_taskrun (
  id SERIAL PRIMARY KEY,
  report_id INT NOT NULL REFERENCES perf_report(id),
  runner_id INT NOT NULL REFERENCES perf_runner(id),
  name VARCHAR(2000) NOT NULL,
  description VARCHAR(5000) NULL,
  warmup_runs BIGINT NULL,
  num_runs BIGINT NULL,
  run_type VARCHAR(100) NULL,
  iscomplete BOOLEAN NOT NULL DEFAULT FALSE,
  iserror BOOLEAN NOT NULL DEFAULT FALSE,
  error VARCHAR(2000) NULL,
  start TIMESTAMP NULL,
  stop TIMESTAMP NULL,
  total_runs BIGINT NULL,
  max_run INT NULL,
  min_run INT NULL,
  avg_run DECIMAL(9,1) NULL,
  rate DECIMAL(9,1) NULL,
  total_run_time BIGINT NULL
);
CREATE INDEX idx_perf_taskrun ON perf_taskrun(name);
CREATE INDEX idx2_perf_taskrun ON perf_taskrun(report_id, runner_id, name);

CREATE TABLE perf_taskextra (
  id SERIAL PRIMARY KEY,
  taskrun_id INT NOT NULL REFERENCES perf_taskrun(id),
  name VARCHAR(2000) NOT NULL,
  value TEXT NULL,
  type VARCHAR(3) NULL,
  unit VARCHAR(100) NULL
);
CREATE INDEX idx_perf_taskextrak ON perf_taskextra(name, unit);
