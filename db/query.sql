
select * from perf_runner;
select * from perf_report;
select * from perf_taskrun;
select * from perf_taskextra;

-- List of reports per hostname
select a.id report_id, a.name report_name, a.start report_date, c.value hostname
from perf_report a
  left join perf_taskrun b on a.id = b.report_id
  left join perf_taskextra c on b.id = c.taskrun_id
where b.name = 'systemInfo' and c.name = 'system.hostname'
order by c.value, a.start;

-- List of latest reports per hostname
select max(a.start) report_date, max(a.id) report_id, c.value hostname
from perf_report a
  left join perf_taskrun b on a.id = b.report_id
  left join perf_taskextra c on b.id = c.taskrun_id
where b.name = 'systemInfo' and c.name = 'system.hostname'
  group by c.value
 ORDER BY c.value;

-- List of taskruns per report id
select b.*, a.id report_id, a.name report_name
from perf_report a
  left join perf_taskrun b on a.id = b.report_id
where a.id = 2
  order by a.id, a.start, b.runner_id, b.name;

-- List of task names
select DISTINCT name from perf_taskrun order by name;

-- List of all tasks with taskextra that has 'Rate'
select a.id, a.name task_name, a.avg_run, a.rate, b.name extra_name, b.value extra_value, b.unit extra_unit, b.type extra_type
from perf_taskrun a
  left join perf_taskextra b ON a.id = b.taskrun_id
where a.report_id = 1 and b.name = 'Rate'
order by a.runner_id, a.name, b.name;

-- List of specific taskname with taskextra
select a.id, a.name task_name, a.avg_run, a.rate, b.name extra_name, b.value extra_value, b.unit extra_unit, b.type extra_type
from perf_taskrun a
  left join perf_taskextra b ON a.id = b.taskrun_id
where a.report_id = 2 and
      a.name in ('testEmptyMethod',
                 'testForLoop',
                 'testHeavyMath',
                 'testUUID',
                 'testParseDate',
                 'testFilesRead',
                 'testFilesWrite')
order by a.runner_id, a.name, b.name;

-- List of system info per report
select a.id report_id, a.name report_name,
       b.name task_name, c.name extra_name, c.value extra_value
from perf_report a
  left join perf_taskrun b on a.id = b.report_id
  left join perf_taskextra c on b.id = c.taskrun_id
where b.name = 'systemInfo'
order by c.name;
