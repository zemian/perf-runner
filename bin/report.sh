#!/usr/bin/env bash
#
# Usage ./report.sh [writer-profiles]
#
# Usage ./report.sh [writer-profiles] [runner-profiles]
#

WRITER=adoc-writer
RUNNER=basic-runner,db-runner,ser-runner
if [[ $# -eq 1 ]]; then
    WRITER=$1
elif [[ $# -eq 2 ]]; then
    WRITER=$1
    RUNNER=$2
fi
JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=$WRITER,$RUNNER"
echo "Using $JAVA_OPTS"
mvn $JAVA_OPTS spring-boot:run
