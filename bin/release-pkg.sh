#!/usr/bin/env bash
#
# Zip up package for remote deployment.
#

# Package deploy app package
PKG=target/perf-runner-app 
if [[ -d $PKG ]]; then
	echo "Removing existing release package $PKG"
	rm -rf $PKG
fi
echo "Creating new release package $PKG"
mkdir -p $PKG
cp -rf bin/release/perf-runner-app/* $PKG
cp target/perf-runner-*-app.jar $PKG
cp src/main/resources/application.properties $PKG

# Record current git HEAD commit sha1 as id
echo "commitid=$(git rev-parse HEAD)" > $PKG/release.properties

zip -r -D "${PKG}.zip" $PKG
echo "Package ${PKG}.zip created"
