
#!/usr/bin/env bash
#
# Invoke a single runner
# Usage ./report-runner.sh <runnerClass>
# Or
# Usage ./report-runner.sh <runnerClass> <springProfiles>
#

if [[ $# -eq 1 ]]; then
    RUNNER=$1
    PROFILES=adoc-writer,basic-runner,db-runner,ser-runner
elif [[ $# -eq 2 ]]; then
    RUNNER=$2
    PROFILES=$1
else
    echo "ERROR: Missing arguments"
    exit 1
fi

JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=$PROFILES -DincludeRunners=$RUNNER"
echo "Using $JAVA_OPTS"
mvn $JAVA_OPTS spring-boot:run
