package com.zemian.perfrunner.runner.ser;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Profile("ser-runner")
@Component
@Order(303)
public class SerJaxbPerfRunner extends BaseSerPerfRunner {
    @Override
    public void run() throws Exception {
        PersonList list = createPersonList();

        JAXBContext jaxb = JAXBContext.newInstance(PersonList.class, Person.class);
        Marshaller marshaller = jaxb.createMarshaller();
        PerfTask task = new PerfTask("testJAXBMarshaller", "Writing file", numOfWarmups, numOfRuns, ctx -> {
            try (FileOutputStream fout = new FileOutputStream(new File(testDir, "personList.dat"))) {
                marshaller.marshal(list, fout);
            }
        });
        task.timeit();

        double rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        double compareRate = runnersSharedData.compareRate("testCsvWrite.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvWrite.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);

        Unmarshaller unmarshaller = jaxb.createUnmarshaller();
        task = new PerfTask("testJAXBUnmarshaller", "Reading file.", numOfWarmups, numOfRuns, ctx -> {
            try (FileInputStream fin = new FileInputStream(new File(testDir, "personList.dat"))) {
                PersonList result = (PersonList) unmarshaller.unmarshal(fin);
                Assert.isTrue(result.getPersonList().size() == numOfListSample, "Result size is not correct");
            }
        });
        task.timeit();

        rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        compareRate = runnersSharedData.compareRate("testCsvRead.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvRead.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);
    }
}
