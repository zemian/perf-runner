package com.zemian.perfrunner.runner.ser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

@Profile("ser-runner")
@Component
@Order(304)
public class SerJsonPerfRunner extends BaseSerPerfRunner {
    @Override
    public void run() throws Exception {
        PersonList list = createPersonList();

        ObjectMapper json = new ObjectMapper();
        PerfTask task = new PerfTask("testJsonWrite", "Writing file", numOfWarmups, numOfRuns, ctx -> {
            try (FileOutputStream fout = new FileOutputStream(new File(testDir, "personList.dat"))) {
                json.writeValue(fout, list);
            }
        });
        task.timeit();

        double rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        double compareRate = runnersSharedData.compareRate("testCsvWrite.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvWrite.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);

        task = new PerfTask("testJsonRead", "Reading file.", numOfWarmups, numOfRuns, ctx -> {
            try (FileInputStream fin = new FileInputStream(new File(testDir, "personList.dat"))) {
                PersonList result = json.readValue(fin, PersonList.class);
                Assert.isTrue(result.getPersonList().size() == numOfListSample, "Result size is not correct");
            }
        });
        task.timeit();

        rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        compareRate = runnersSharedData.compareRate("testCsvRead.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvRead.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);
    }
}
