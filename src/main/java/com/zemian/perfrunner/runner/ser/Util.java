package com.zemian.perfrunner.runner.ser;

public class Util {
    public static Person create() {
        Person a = new Person("John", "Doe");
        a.setAge(21);
        a.setSpouse(new Person("Jane", "Doe"));
        return a;
    }
}
