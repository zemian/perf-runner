package com.zemian.perfrunner.runner.ser;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Profile("ser-runner")
@Component
@Order(302)
public class SerJavaSerPerfRunner extends BaseSerPerfRunner {

    @Override
    public void run() throws Exception {
        PersonList list = createPersonList();

        PerfTask task = new PerfTask("testJavaSerialization", "Writing file", numOfWarmups, numOfRuns, ctx -> {
            try (FileOutputStream fout = new FileOutputStream(new File(testDir, "personList.dat"))) {
                ObjectOutputStream out = new ObjectOutputStream(fout);
                out.writeObject(list);
                out.close();
            }
        });
        task.timeit();

        double rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        double compareRate = runnersSharedData.compareRate("testCsvWrite.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvWrite.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);

        task = new PerfTask("testJavaDeserialization", "Reading file.", numOfWarmups, numOfRuns, ctx -> {
            try (FileInputStream fin = new FileInputStream(new File(testDir, "personList.dat"))) {
                ObjectInputStream in = new ObjectInputStream(fin);
                PersonList result = (PersonList) in.readObject();
                Assert.isTrue(result.getPersonList().size() == numOfListSample, "Result size is not correct");
            }
        });
        task.timeit();

        rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        compareRate = runnersSharedData.compareRate("testCsvRead.rate", rate);
        task.addExtraField("CompareRate", compareRate, "testCsvRead.rate");
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);
    }
}
