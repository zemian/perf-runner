package com.zemian.perfrunner.runner.ser;

import com.zemian.perfrunner.PerfRunner;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseSerPerfRunner extends PerfRunner {

    @Value("${perfrunner.BaseSerPerfRunner.testDir:target/ser-test}")
    protected File testDir;

    @Value("${perfrunner.BaseSerPerfRunner.numOfWarmups:10}")
    protected int numOfWarmups;

    @Value("${perfrunner.BaseSerPerfRunner.numOfRuns:100}")
    protected int numOfRuns;

    @Value("${perfrunner.BaseSerPerfRunner.numOfListSample:10000}")
    protected int numOfListSample;

    @Override
    public void init() throws Exception {
        testDir.mkdirs();
    }

    @Override
    public void destroy() throws Exception {
        FileUtils.deleteDirectory(testDir);
    }

    protected PersonList createPersonList() {
        PersonList result = new PersonList();
        List<Person> list = new ArrayList<>();
        for (int i = 0; i < numOfListSample; i++) {
            Person a = Util.create();
            list.add(a);
        }
        result.setPersonList(list);
        return result;
    }
}
