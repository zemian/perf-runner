package com.zemian.perfrunner.runner.ser;

import com.zemian.perfrunner.PerfTask;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;

@Profile("ser-runner")
@Component
@Order(301)
public class SerCsvPerfRunner extends BaseSerPerfRunner {
    @Override
    public void run() throws Exception {
        PersonList list = createPersonList();
        double rate;

        PerfTask task = new PerfTask("testCsvWrite", "Writing file", numOfWarmups, numOfRuns, ctx -> {
            try (BufferedWriter fout = new BufferedWriter(new FileWriter(new File(testDir, "personList.dat")))) {
                for (Person p : list.getPersonList()) {
                    fout.write(p.toRecord() + "\n");
                }
            }
        });
        task.timeit();

        rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        writer.writeTask(task);
        runnersSharedData.add("testCsvWrite.rate", rate);

        ArrayList<Person> resultList = new ArrayList<>();
        PersonList result = new PersonList();
        result.setPersonList(resultList);
        task = new PerfTask("testCsvRead", "Reading file.", numOfWarmups, numOfRuns, ctx -> {
            try (BufferedReader fin = new BufferedReader(new FileReader(new File(testDir, "personList.dat")))) {
                String line = null;
                while ((line = fin.readLine()) != null) {
                    if (StringUtils.isEmpty(line))
                        continue;
                    Person p = Person.fromRecord(line);
                    resultList.add(p);
                }
            }
            Assert.isTrue(result.getPersonList().size() == numOfListSample, "Result size is not correct");
            result.getPersonList().clear();
        });
        task.timeit();

        rate = (double)(numOfListSample * numOfRuns) / task.getTotalRunTime();
        task.addExtraField("NumOfListSample", numOfListSample);
        task.addExtraField("Rate", rate, "objects/ms");
        runnersSharedData.add("testCsvRead.rate", rate);

        writer.writeTask(task);
    }
}
