package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Measure the speed on writing and reading files into one or more directories. This runner can help measure
 * speed on various mounted paths that you have as testDirectories by writing various size of sample text files.
 */
@Profile("basic-runner")
@Component
@Order(101)
public class DiskFilePerfRunner extends PerfRunner {
    private static final Logger LOG = getLogger(DiskFilePerfRunner.class);

    @Value("${perfrunner.DiskFilePerfRunner.testDirectories:target/disk-file-test}")
    private List<File> testDirectories;

    @Value("${perfrunner.DiskFilePerfRunner.numOfWarmupRuns:10}")
    private int numOfWarmupRuns;

    @Value("${perfrunner.DiskFilePerfRunner.numOfRuns:100}")
    private int numOfRuns;


    private String tinySample;
    private String smallSample;
    private String mediumSample;
    private String largeSample;

    @Override
    public void init() {
        // Init directory.
        for (File dir : testDirectories) {
            // Ensure directory exist
            if (!dir.exists()) {
                LOG.debug("Making test dir={}", dir);
                dir.mkdirs();
            }
        }

        // Init sample text
        tinySample = "Hello World";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10_000; i++) {
            sb.append("x");
        }
        smallSample = sb.toString();

        sb = new StringBuilder();
        for (int i = 0; i < 1_000_000; i++) {
            sb.append("x");
        }
        mediumSample = sb.toString();

        sb = new StringBuilder();
        for (int i = 0; i < 10_000_000; i++) {
            sb.append("x");
        }
        largeSample = sb.toString();
    }

    @Override
    public void destroy() {
        // delete directory.
        for (File dir : testDirectories) {
            if (dir.exists()) {
                LOG.debug("Deleting test dir={}", dir);
                try {
                    FileUtils.deleteDirectory(dir);
                } catch (IOException e) {
                    LOG.warn("Failed to delete dir " + dir);
                }
            }
        }
    }

    @Override
    public void run() throws Exception {
        for (File dir : testDirectories) {
            testFileAccess(dir);
        }
    }

    private void testFileAccess(File dir) {
        Set<File> files = new HashSet<>();
        String[] samples = new String[]{tinySample, smallSample, mediumSample, largeSample};

        try {
            // Test file write
            PerfTask task = new PerfTask("testFilesWrite", numOfWarmupRuns, numOfRuns, ctx -> {
                int i = 0;
                for (String sample : samples) {
                    int type = ++i % samples.length;
                    File file = new File(dir, "sample_" + (type) + ".txt");
                    files.add(file);
                    try (FileWriter writer = new FileWriter(file)) {
                        writer.write(sample);
                    }
                    LOG.trace("Created file {}", file);
                }
            });
            task.timeit();

            Assert.isTrue(files.size() == samples.length, "Not all sample files are written!");

            // Add more result notes to task
            long sampleSize = Arrays.stream(samples).mapToLong(a -> a.length()).sum();
            long totalByteSize = sampleSize * task.getTotalRuns();
            double bytesPerMillis = totalByteSize / (task.getTotalRunTime() * 1.0);
            task.addExtraField("Tested Directory", dir.getPath());
            task.addExtraField("SampleFileSize", sampleSize, "bytes");
            task.addExtraField("Number of Samples", samples.length);
            task.addExtraField("Number of Files Written", samples.length * task.getTotalRuns());
            task.addExtraField("Rate", bytesPerMillis, "bytes/ms");

            writer.writeTask(task);

            runnersSharedData.add("testFilesWrite.rate", bytesPerMillis);

            // Test file read
            task = new PerfTask("testFilesRead", numOfWarmupRuns, numOfRuns, ctx -> {
                for (File file : files) {
                    try (FileReader reader = new FileReader(file)) {
                        BufferedReader br = new BufferedReader(reader);
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            line.length();
                        }
                    }
                    LOG.trace("Read file {}", file);
                }
            });
            task.timeit();

            bytesPerMillis = totalByteSize / (task.getTotalRunTime() * 1.0);
            double compareRate = runnersSharedData.compareRate("testFilesWrite.rate", bytesPerMillis);
            task.addExtraField("Tested Directory", dir.getPath());
            task.addExtraField("Number of Samples", samples.length);
            task.addExtraField("Number of Files Read", files.size() * task.getTotalRuns());
            task.addExtraField("Rate", bytesPerMillis, "bytes/ms");
            task.addExtraField("CompareRate", compareRate , "testFilesWrite.rate");
            writer.writeTask(task);
        } finally {
            // Ensure we delete test files in case or error
            for (File file : files) {
                if (file.exists()) {
                    file.delete();
                    LOG.debug("Removing unprocessed test file {}", file);
                }
            }
        }
    }
}
