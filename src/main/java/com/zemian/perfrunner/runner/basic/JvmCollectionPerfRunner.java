package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.DCounter;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import com.zemian.perfrunner.Util;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Comparing JVM Colection Framework Speed
 */
@Profile("basic-runner")
@Component
@Order(3)
public class JvmCollectionPerfRunner extends PerfRunner {

    int numOfItems = 2_000_000;
    int numOfWarmups = 10;
    int numOfRuns = 100;

    @Override
    public void run() throws Exception {
        testColArray();
        testColArrayList();
        testColLinkedList();
        testColHashMap();
        testColConcurrentHashMap();
        testColSynchronizedHashMap();
        testColLinkedHashMap();
        testColTreeMap();
        testColHashSet();
        testColLinkedHashSet();
        testColTreeSet();
    }

    public void testColArray() {
        String[] array = new String[numOfItems];
        DCounter counter = new DCounter();
        add: {
            PerfTask task = new PerfTask("testColArray_add", "Insert of " + Util.DF.format(numOfItems) + " items.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (int i = 0; i < numOfItems; i++) {
                    array[i] = "TEST" + i;
                }
                // Save the last run for later use
                counter.o = array;
            });
            task.timeit();
            task.addExtraField("Rate", array.length / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        loop: {
            PerfTask task = new PerfTask("testColArray_loop", "Loop all by iterator per run.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (String elem : array) {
                    elem.length();
                }
            });
            task.timeit();
            task.addExtraField("Rate", array.length / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        get: {
            PerfTask task = new PerfTask("testColArray_get", "Lookup by index for all items.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (int i = 0; i < array.length; i++) {
                    array[i].length();
                }
            });
            task.timeit();
            task.addExtraField("Rate", array.length / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        update: {
            PerfTask task = new PerfTask("testColArray_update", "Update item by index.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (int i = 0; i < array.length; i++) {
                    array[i] = "UPDATED" + i;
                }
            });
            task.timeit();
            task.addExtraField("Rate", array.length / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        copy: {
            PerfTask task = new PerfTask("testColArray_copy", "Copy items.",
                    numOfWarmups, numOfRuns, ctx -> {
                String[] copy = Arrays.copyOf(array, array.length);
                Assert.isTrue(copy.length == array.length, "Copied size not the same");
            });
            task.timeit();
            task.addExtraField("Rate", array.length / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        delete: {
            PerfTask task = new PerfTask("testColArray_delete", "Removing items from tail.",
                    0, 1, ctx -> {
                for (int i = array.length - 1; i >= 0; i--) {
                    array[i] = null;
                }
            });
            task.timeit();
            task.addExtraField("Rate", numOfItems / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
    }

    public void testColArrayList() {
        testColList(new ArrayList<>());
    }

    public void testColLinkedList() {
        testColList(new LinkedList<>());
    }

    public void testColList(List<String> list) {
        String taskNamePrefix = "testCol" + list.getClass().getSimpleName();
        add: {
            PerfTask task = new PerfTask(taskNamePrefix + "_add", "Insert of " + Util.DF.format(numOfItems) + " items.",
                    numOfWarmups, numOfRuns, ctx -> {
                list.clear();
                for (int i = 0; i < numOfItems; i++) {
                    list.add("TEST" + i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", list.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        loop: {
            PerfTask task = new PerfTask(taskNamePrefix + "_loop", "Loop all by iterator per run.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (String elem : list) {
                    elem.length();
                }
            });
            task.timeit();
            task.addExtraField("Rate", list.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        get: {
            PerfTask task;
            if (list instanceof LinkedList) {
                // We know linked list can't handle large direct get() as it's extremely slow.
                int testSize = 5000;
                task = new PerfTask(taskNamePrefix + "_get", "Lookup by index for " + Util.DF.format(testSize) + " items.",
                        numOfWarmups, numOfRuns, ctx -> {
                    for (int i = 0; i < testSize; i++) {
                        list.get(i).length();
                    }
                });
            } else {
                task = new PerfTask(taskNamePrefix + "_get", "Lookup by index for all items.",
                        numOfWarmups, numOfRuns, ctx -> {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).length();
                    }
                });
            }
            task.timeit();
            task.addExtraField("Rate", list.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        update: {
            PerfTask task;
            if (list instanceof LinkedList) {
                // We know linked list can't handle large direct get() as it's extremely slow.
                int testSize = 5000;
                task = new PerfTask(taskNamePrefix + "_update", "Update item by index for " + Util.DF.format(testSize) + " items.",
                        numOfWarmups, numOfRuns, ctx -> {
                    for (int i = 0; i < testSize; i++) {
                        list.set(i, "UPDATED" + i);
                    }
                });
            } else {
                task = new PerfTask(taskNamePrefix + "_update", "Update item by index for all items",
                        numOfWarmups, numOfRuns, ctx -> {
                    for (int i = 0; i < list.size(); i++) {
                        list.set(i, "UPDATED" + i);
                    }
                });
            }
            task.timeit();
            task.addExtraField("Rate", list.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        copy: {
            PerfTask task = new PerfTask(taskNamePrefix + "_copy", "Copy (shallow) items.",
                    numOfWarmups, numOfRuns, ctx -> {
                List<String> copy = new ArrayList<>(list);
                Assert.isTrue(copy.size() == list.size(), "Copied size not the same");
            });
            task.timeit();
            task.addExtraField("Rate", list.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        delete: {
            PerfTask task = new PerfTask(taskNamePrefix + "_delete", "Removing items from tail.",
                    0, 1, ctx -> {
                for (int i = list.size() - 1; i >= 0; i--) {
                    list.remove(i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", numOfItems / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
    }

    public void testColHashMap() {
        testColMap(new HashMap<>());
    }

    public void testColConcurrentHashMap() {
        testColMap(new ConcurrentHashMap<>());
    }

    public void testColSynchronizedHashMap() {
        testColMap(Collections.synchronizedMap(new HashMap<>()));
    }

    public void testColLinkedHashMap() {
        testColMap(new LinkedHashMap<>());
    }

    public void testColTreeMap() {
        testColMap(new TreeMap<>());
    }

    public void testColMap(Map<String, String> map) {
        String taskNamePrefix = "testCol" + map.getClass().getSimpleName();
        DCounter counter = new DCounter();
        add: {
            PerfTask task = new PerfTask(taskNamePrefix + "_add", "Insert of " + Util.DF.format(numOfItems) + " items.",
                    numOfWarmups, numOfRuns, ctx -> {
                map.clear();
                for (int i = 0; i < numOfItems; i++) {
                    map.put("TEST" + i, "" + i);
                }
                // Save the last run for later use
                counter.o = map;
            });
            task.timeit();
            task.addExtraField("Rate", map.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        loop: {
            PerfTask task = new PerfTask(taskNamePrefix + "_loop", "Loop all by iterator per run.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    entry.getKey();
                    entry.getValue();
                }
            });
            task.timeit();
            task.addExtraField("Rate", map.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        get: {
            PerfTask task = new PerfTask(taskNamePrefix + "_get", "Lookup item by key.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (String name : map.keySet()) {
                    map.get(name).length();
                }
            });
            task.timeit();
            task.addExtraField("Rate", map.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        update: {
            Set<String> keys = new HashSet<>(map.keySet());
            counter.i = 0;
            PerfTask task = new PerfTask(taskNamePrefix + "_update", "Update item by key.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (String name : keys) {
                    map.put(name, "" + 100 + counter.i++);
                }
            });
            task.timeit();
            task.addExtraField("Rate", map.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        copy: {
            PerfTask task = new PerfTask(taskNamePrefix + "_copy", "Copy (shallow) items.",
                    numOfWarmups, numOfRuns, ctx -> {
                Map<String, String> copy = new HashMap<>(map);
                Assert.isTrue(copy.size() == map.size(), "Copied size not the same");
            });
            task.timeit();
            task.addExtraField("Rate", map.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        delete: {
            Set<String> keys = new HashSet<>(map.keySet());
            counter.i = 0;
            PerfTask task = new PerfTask(taskNamePrefix + "_delete", "Removing item by key.",
                    0, 1, ctx -> {
                for (String name : keys) {
                    map.remove(name);
                }
            });
            task.timeit();
            task.addExtraField("Rate", numOfItems / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
    }

    public void testColHashSet() {
        testColSet(new HashSet<>());
    }

    public void testColLinkedHashSet() {
        testColSet(new LinkedHashSet<>());
    }

    public void testColTreeSet() {
        testColSet(new TreeSet<>());
    }

    public void testColSet(Set<String> set) {
        String taskNamePrefix = "testCol" + set.getClass().getSimpleName();
        add: {
            PerfTask task = new PerfTask(taskNamePrefix + "_add", "Insert of " + Util.DF.format(numOfItems) + " items.",
                    numOfWarmups, numOfRuns, ctx -> {
                set.clear();
                for (int i = 0; i < numOfItems; i++) {
                    set.add("TEST" + i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", set.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        loop: {
            PerfTask task = new PerfTask(taskNamePrefix + "_loop", "Loop all by iterator per run.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (String elem : set) {
                    elem.length();
                }
            });
            task.timeit();
            task.addExtraField("Rate", set.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        get: {
            PerfTask task = new PerfTask(taskNamePrefix + "_get", "Lookup by item by key.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (int i = 0; i < set.size(); i++) {
                    set.contains("TEST" + i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", set.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        update: {
            PerfTask task = new PerfTask(taskNamePrefix + "_update", "Update item by adding duplicated key.",
                    numOfWarmups, numOfRuns, ctx -> {
                for (int i = 0; i < set.size(); i++) {
                    set.add("TEST" + i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", set.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        copy: {
            PerfTask task = new PerfTask(taskNamePrefix + "_copy", "Copy (shallow) items.",
                    numOfWarmups, numOfRuns, ctx -> {
                Set<String> copy = new HashSet<>(set);
                Assert.isTrue(copy.size() == set.size(), "Copied size not the same");
            });
            task.timeit();
            task.addExtraField("Rate", set.size() / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
        delete: {
            PerfTask task = new PerfTask(taskNamePrefix + "_delete", "Removing items from tail.",
                    0, 1, ctx -> {
                for (int i = set.size() - 1; i >= 0; i--) {
                    set.remove("TEST" + i);
                }
            });
            task.timeit();
            task.addExtraField("Rate", numOfItems / task.getAvgRunTime(), "items/ms");
            writer.writeTask(task);
        }
    }
}
