package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.AppException;
import com.zemian.perfrunner.DCounter;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Measure some basic JVM performance such as invoking method, calling random generator and Math operations etc.
 */
@Profile("basic-runner")
@Component
@Order(2)
public class JvmBasicPerfRunner extends PerfRunner {

    @Override
    public void run() throws Exception {
        testEmptyMethod();
        testCompareByGT();
        testCompareByGET();
        testRandDouble();
        testHeavyMath();
        testUUID();
        testParseDate();
        testDoubleCastVsMult();
        testExceptionCost();
        testStringSplit();
        testTightLoop();
    }

    public void testTightLoop() {
        int recursiveLoopSize = 300;
        int loopSize = recursiveLoopSize * 3500;
        for_loop: {
            DCounter counter = new DCounter();
            counter.d = 2.0;
            PerfTask task = new PerfTask("testTightLoop_for",
                    "Tight loop of " + loopSize + " executions of math x^2 + y^2 operation.", 10, 100, ctx -> {
                for (int i = 0; i < loopSize; i++) {
                    double result = Math.pow(Math.sin(counter.d), 2.0) + Math.pow(Math.tan(counter.d), 2.0);
                    counter.d += result;
                    counter.l++;
                }
            });
            task.timeit();
            double loopsPerMillis = ((double) counter.l) / task.getTotalRunTime();
            task.addExtraField("Rate", loopsPerMillis, "loops/ms");
            runnersSharedData.add("testTightLoop_for.rate", loopsPerMillis);
            writer.writeTask(task);
        }

        while_loop: {
            DCounter counter = new DCounter();
            counter.d = 2.0;
            PerfTask task = new PerfTask("testTightLoop_while",
                    "Tight loop of " + loopSize + " executions of math x^2 + y^2 operation.", 10, 100, ctx -> {
                int i = 0;
                while (i++ < loopSize) {
                    double result = Math.pow(Math.sin(counter.d), 2.0) + Math.pow(Math.tan(counter.d), 2.0);
                    counter.d += result;
                    counter.l++;
                }
            });
            task.timeit();
            double loopsPerMillis = ((double)counter.l) / task.getTotalRunTime();
            task.addExtraField("Rate", loopsPerMillis, "loops/ms");
            task.addExtraField("CompareRate",
                    runnersSharedData.compareRate("testTightLoop_for.rate", loopsPerMillis), "testTightLoop_for.rate");
            writer.writeTask(task);
        }

        recursive_loop: {
            DCounter counter = new DCounter();
            counter.d = 2.0;
            PerfTask task = new PerfTask("testTightLoop_recursive",
                    "Tight loop of " + loopSize + " executions of math x^2 + y^2 operation.", 10, 100, ctx -> {
                // Recursive can not use large "loopSize", so we divide it up.
                for (int i = 0, max = loopSize / recursiveLoopSize; i < max; i++) {
                    recursive(counter, recursiveLoopSize);
                }
            });
            task.timeit();
            double loopsPerMillis = ((double)counter.l) / task.getTotalRunTime();
            task.addExtraField("Rate", loopsPerMillis, "loops/ms");
            task.addExtraField("CompareRate",
                    runnersSharedData.compareRate("testTightLoop_for.rate", loopsPerMillis), "testTightLoop_for.rate");
            writer.writeTask(task);
        }
    }

    private void recursive(DCounter counter, int recurCount) {
        double result = Math.pow(Math.sin(counter.d), 2.0) + Math.pow(Math.tan(counter.d), 2.0);
        counter.d += result;
        counter.l++;
        if (recurCount > 0)
            recursive(counter, recurCount - 1);
    }

    public void testStringSplit() {
        int numOfWarmups = 1000, numOfRuns = 1_000_000;
        PerfTask task = new PerfTask("testStringSplit_jdk-string", "java.lang.String.split()",
                numOfWarmups, numOfRuns, ctx -> {
            "one, two, three, four, five".split(", ");
        });
        task.timeit();
        runnersSharedData.add("testStringSplit_jdk-string.rate", task.getRate());
        writer.writeTask(task);

        //NOTE: StringTokenizer is deemed to be deprecated by javadoc and encourage to use String.split
        // instead, however the speed is still better!
        task = new PerfTask("testStringSplit_jdk-tokenizer", "java.util.StringTokenizer",
                numOfWarmups, numOfRuns, ctx -> {
            StringTokenizer st = new StringTokenizer("one, two, three, four, five", ", ");
            while (st.hasMoreTokens()) {
                st.nextToken();
            }
        });
        task.timeit();
        task.addExtraField("CompareRate",
                runnersSharedData.compareRate("testStringSplit_jdk-string.rate", task.getRate()),
                "testStringSplit_jdk-string.rate");
        writer.writeTask(task);

        task = new PerfTask("testStringSplit_commons-lang3", "org.apache.commons.lang3.StringUtils.split()",
                numOfWarmups, numOfRuns, ctx -> {
            StringUtils.split("one, two, three, four, five", ", ");
        });
        task.timeit();
        task.addExtraField("CompareRate",
                runnersSharedData.compareRate("testStringSplit_jdk-string.rate", task.getRate()),
                "testStringSplit_jdk-string.rate");
        writer.writeTask(task);
    }

    public void testExceptionCost() {
        PerfTask task = new PerfTask("testExceptionCost", "The cost of raising exception", 1000, 0, ctx -> {
            try {
                throw new AppException("Just a test");
            } catch (Exception e) {
                // Ignore.
            }
        });
        task.timeitForMillis(10_000L);
        writer.writeTask(task);
    }

    public void testDoubleCastVsMult() {
        {
            PerfTask task = new PerfTask("testDoubleCast", "Cast double promotion.", 1000, 0, ctx -> {
                long a = 1000L;
                double b = (double) a;
            });
            task.timeitForMillis(10_000L);
            runnersSharedData.add("testDoubleCast.rate", task.getRate());
            writer.writeTask(task);
        }
        {
            PerfTask task = new PerfTask("testDoubleMult1.0", "Multiply 1.0", 1000, 0, ctx -> {
                long a = 1000L;
                double b = 1.0 * a;
            });
            task.timeitForMillis(10_000L);
            task.addExtraField("CompareRate",
                    runnersSharedData.compareRate("testDoubleCast.rate", task.getRate()), "testDoubleCast.rate");
            writer.writeTask(task);
        }
    }

    public void testParseDate() {
        PerfTask task = new PerfTask("testParseDate",
                "Parsing date with date time and timezone value.", 1000, 0, ctx -> {
            String dtInput = "2017-10-14T22:18:30.497-0400";
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date dt = df.parse(dtInput);
            df.format(dt);
        });
        task.timeitForMillis(10_000L);
        writer.writeTask(task);
    }

    public void testHeavyMath() {
        DCounter counter = new DCounter();
        counter.d = 2.0;
        PerfTask task = new PerfTask("testHeavyMath",
                "Performing x^2 + y^2 math operations", 1000, 0, ctx -> {
            double result = Math.pow(Math.sin(counter.d), 2.0) + Math.pow(Math.tan(counter.d), 2.0);
            counter.d += result;
        });
        task.timeitForMillis(10_000L);
        writer.writeTask(task);
    }

    public void testCompareByGT() {
        DCounter counter = new DCounter();
        PerfTask task = new PerfTask("testCompareByGT",
                "Performing '>' operation.", 1000, 0, ctx -> {
            if (counter.d > 1000.0) {
                counter.d++;
            }
        });
        task.timeitForMillis(10_000L);
        runnersSharedData.add("testCompareByGT.rate", task.getRate());
        writer.writeTask(task);
    }

    public void testCompareByGET() {
        DCounter counter = new DCounter();
        PerfTask task = new PerfTask("testCompareByGET",
                "Performing '>=' operation.", 1000, 0, ctx -> {
            if (counter.d >= 1000.0) {
                counter.d++;
            }
        });
        task.timeitForMillis(10_000L);
        task.addExtraField("CompareRate",
                runnersSharedData.compareRate("testCompareByGT.rate", task.getRate()), "testCompareByGT.rate");
        writer.writeTask(task);
    }

    public void testEmptyMethod() {
        DCounter c = new DCounter();
        PerfTask task = new PerfTask("testEmptyMethod",
                "Calling single empty method.", 1000, 0, ctx -> {
            c.emptyMethod();
        });
        task.timeitForMillis(10_000L);
        runnersSharedData.add("testEmptyMethod.rate", task.getRate());
        writer.writeTask(task);
    }

    public void testUUID() {
        PerfTask task = new PerfTask("testUUID",
                "Generating UUID random string.", 1000, 0, ctx -> {
            UUID.randomUUID();
        });
        task.timeitForMillis(10_000L);
        writer.writeTask(task);
    }

    public void testRandDouble() {
        DCounter c = new DCounter();
        PerfTask task = new PerfTask("testRandDouble",
                "Generating a random double and doing a '>=' comparison.", 1000, 0, ctx -> {
            if (Math.random() >= 0.5) {
                c.emptyMethod();
            }
        });
        task.timeitForMillis(10_000L);
        writer.writeTask(task);
    }
}
