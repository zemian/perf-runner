package com.zemian.perfrunner.runner;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Gather JVM and system information and write to perf report.
 */
@Component
@Order(1)
public class JvmSysInfoPerfRunner extends PerfRunner {
    @Override
    public void run() throws Exception {
        // Create a dummy task just so we can generate report.
        PerfTask task = new PerfTask("systemInfo", "Collect system information.", ctx -> {
            Runtime runtime = Runtime.getRuntime();
            NumberFormat nf = DecimalFormat.getInstance();

            // System OS info
            ctx.task.addExtraField("system.hostname", InetAddress.getLocalHost().getHostName());
            ctx.task.addExtraField("system.ip", InetAddress.getLocalHost().getHostAddress());
            ctx.task.addExtraField("system.availableProcessors", runtime.availableProcessors());

            // Get only small set of system props
            Map<String, String> sysInfo = new TreeMap<>();
            Properties sysProps = System.getProperties();
            for (String name : sysProps.stringPropertyNames()) {
                if (name.startsWith("os") ||
                        name.startsWith("sun.cpu") ||
                        name.startsWith("sun.os") ||
                        name.startsWith("java.vm") ||
                        name.startsWith("user.")) {
                    sysInfo.put(name, sysProps.getProperty(name));
                }
            }

            // Write SystemInfo props
            for (Map.Entry<String, String> entry : sysInfo.entrySet()) {
                ctx.task.addExtraField(entry.getKey(), entry.getValue());
            }

            // Write memory available.
            ctx.task.addExtraField("jvm.maxMemory", nf.format(runtime.maxMemory()));
            ctx.task.addExtraField("jvm.totalMemory", nf.format(runtime.totalMemory()));
            ctx.task.addExtraField("jvm.freeMemory", nf.format(runtime.freeMemory()));

            // Physical memory
            try {
                MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
                Object attribute = mBeanServer.getAttribute(new ObjectName("java.lang","type","OperatingSystem"), "TotalPhysicalMemorySize");
                long memSize = Long.parseLong(attribute.toString());
                ctx.task.addExtraField("system.physicalmemory", nf.format(memSize));
            } catch (Exception e) {
                // Ignore.
            }

            // Disk info
            File[] roots = File.listRoots();
            int index = 1;
            for (File root : roots) {
                ctx.task.addExtraField("filesystem.root." + index, root.getAbsolutePath());
                ctx.task.addExtraField("filesystem.root." + index + ".total.space", nf.format(root.getTotalSpace()));
                ctx.task.addExtraField("filesystem.root." + index + ".free.space", nf.format(root.getFreeSpace()));
            }

            // Get time related values
            ctx.task.addExtraField("time.currentdate", new Date());
            ctx.task.addExtraField("time.currentmillis", System.currentTimeMillis());
            ctx.task.addExtraField("time.currentnano", System.nanoTime());
        });
        task.timeit();
        writer.writeTask(task);
    }
}
