package com.zemian.perfrunner.runner.db;

import com.zemian.perfrunner.DCounter;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Performance performance tests on basic jdbc operations. DB user must have create and drop table
 * access rights.
 */
@Profile("db-runner")
@Component
@Order(201)
public class DbJdbcPerfRunner extends PerfRunner {
    private static final Logger LOG = getLogger(DbJdbcPerfRunner.class);

    // For testing direct connections only
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String user;
    @Value("${spring.datasource.password}")
    private String password;

    @Autowired
    private JdbcTemplate jdbc;

    @Value("${perfrunner.DbJdbcPerfRunner.sqlProps:classpath:DbJdbcPerfRunner-sql.xml}")
    private Resource sqlPropsRes;
    private Properties sqlProps;

    private int numOfWarmups = 10;
    private int numOfRuns = 100;

    @Override
    public void init() throws Exception {
        sqlProps = new Properties();
        try (InputStream in = sqlPropsRes.getInputStream()) {
            sqlProps.loadFromXML(in);
        }
    }

    @Override
    public void run() throws Exception {
        testConn();
        testValidateQuery();
        try {
            testCreateTable();
            testInserts();
            testSelects();
            testUpdates();
            testDeletes();
        } catch (Exception e) {
            LOG.error("Failed to test create and other table tests.", e);
        } finally {
            // Ensure we drop table.
            String dropSql = sqlProps.getProperty("drop").trim();
            jdbc.update(dropSql);
        }
    }

    public void testConn() throws Exception {
        PerfTask task = new PerfTask("testConn", "Jdbc connection cost.",
                numOfWarmups, numOfRuns, ctx -> {
            Connection conn = DriverManager.getConnection(url, user, password);
            closeWithoutException(conn);
        });
        task.timeit();
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/conn");

        // Record JDBC info
        try (Connection conn = DriverManager.getConnection(url, user, password)) {
            DatabaseMetaData md = conn.getMetaData();
            task.addExtraField("URL", md.getURL());
            task.addExtraField("DatabaseProductName", md.getDatabaseProductName());
            task.addExtraField("DatabaseProductVersion", md.getDatabaseProductVersion());
            task.addExtraField("DriverName", md.getDriverName());
            task.addExtraField("DriverVersion", md.getDriverVersion());
        }
        writer.writeTask(task);
    }

    private void testValidateQuery() {
        DCounter counter = new DCounter();
        String sql = sqlProps.getProperty("validate").trim();
        PerfTask task = new PerfTask("testValidateQuery", numOfWarmups, numOfRuns, ctx -> {
            List<Map<String, Object>> rows = jdbc.queryForList(sql);
            counter.l = rows.size();
        });
        task.timeit();

        task.addExtraField("Query", sql);
        task.addExtraField("Result Rows Fetched", counter.l);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);
    }

    private void testCreateTable() {
        List<Long> createTimes = new ArrayList<>();
        String createSql = sqlProps.getProperty("create").trim();
        String dropSql = sqlProps.getProperty("drop").trim();
        PerfTask task = new PerfTask("testCreateDropTable", "Create and drop table per run.",
                numOfWarmups, numOfRuns, ctx -> {
            long t1 = System.currentTimeMillis();
            jdbc.update(createSql);
            if (ctx.runType != PerfTask.RunType.WARMUP)
                createTimes.add(System.currentTimeMillis() - t1);

            jdbc.update(dropSql);
        });
        task.timeit();

        long totalCreateTime = createTimes.stream().mapToLong(a -> a).sum();
        double avgCreateTime = createTimes.stream().mapToLong(a -> a).average().getAsDouble();
        long totalDropTime = task.getTotalRunTime() - totalCreateTime;
        double avgDropTime = task.getAvgRunTime() - avgCreateTime;

        task.addExtraField("Query Create", createSql);
        task.addExtraField("Query Drop", dropSql);
        task.addExtraField("Total Create Time", totalCreateTime);
        task.addExtraField("Avg Create Time", avgCreateTime);
        task.addExtraField("Total Drop Time", totalDropTime);
        task.addExtraField("Avg Drop Time", avgDropTime);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);

        // Ensure we create table lastto ensure reset of the tests can work on.
        jdbc.update(createSql);
    }

    private void testInserts() {
        String[] svalSamples = new String[]{"one", "two", "three", "four", "five"};
        DCounter counter = new DCounter();
        String sql = sqlProps.getProperty("insert").trim();
        PerfTask task = new PerfTask("testSqlInserts", "Insert new record",
                numOfWarmups, numOfRuns, ctx -> {
            counter.l++;
            counter.d++;
            counter.o2 = new Timestamp(System.currentTimeMillis());
            String sval = svalSamples[counter.i++ % svalSamples.length];
            jdbc.update(sql, counter.l, sval, counter.d, counter.o2);
        });
        task.timeit();

        task.addExtraField("Query", sql);
        task.addExtraField("Insert Size Per Run", 1);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);
    }

    private void testUpdates() {
        DCounter counter = new DCounter();
        counter.d = 1.0;
        String sql = sqlProps.getProperty("update").trim();
        PerfTask task = new PerfTask("testSqlUpdates", "Updating existinng record",
                numOfWarmups, numOfRuns, ctx -> {
            counter.i = (counter.i++ % 10) + 1;
            counter.l2 += jdbc.update(sql, "TEST", counter.d++, counter.i);
        });
        task.timeit();

        task.addExtraField("Query", sql);
        task.addExtraField("Update Size Per Run", 1);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);
    }

    private void testDeletes() {
        DCounter counter = new DCounter();
        String sql = sqlProps.getProperty("delete").trim();
        PerfTask task = new PerfTask("testSqlDeletes", "Deleting records.",
                numOfWarmups, numOfRuns, ctx -> {
            if (ctx.runType != PerfTask.RunType.WARMUP) {
                jdbc.update(sql, ++counter.i);
            }
        });
        task.timeit();

        task.addExtraField("Query", sql);
        task.addExtraField("Delete Size Per Run", 1);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);
    }

    private void testSelects() {
        int numOfWarmups = 1000;
        int numOfSelects = 5000;
        DCounter counter = new DCounter();

        // We have 5 stored queries to test in similar way
        for (int i = 1; i <= 5; i++) {
            String key = "select" + i;
            String sql = sqlProps.getProperty(key).trim();
            PerfTask task = new PerfTask("testSqlSelects_" + key, numOfWarmups, numOfSelects, ctx -> {
                List<Map<String, Object>> rows = jdbc.queryForList(sql);
                counter.l = rows.size();
            });
            task.timeit();

            task.addExtraField("Query", sql);
            task.addExtraField("Result Rows Fetched", counter.l);
            task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
            writer.writeTask(task);
        }

        // Test 6
        {
            int sqlParam = 500;
            String key = "select6";
            String sql = sqlProps.getProperty(key).trim();
            counter.l2 = 0L;
            PerfTask task = new PerfTask("testSql_" + key, numOfWarmups, numOfSelects, ctx -> {
                List<Map<String, Object>> rows = jdbc.queryForList(sql, sqlParam);
                counter.l = rows.size();
            });
            task.timeit();

            task.addExtraField("Query", sql);
            task.addExtraField("Result Rows Fetched", counter.l);
            task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
            writer.writeTask(task);
        }

        // Test 7
        {
            String sqlParam = "two";
            String key = "select7";
            String sql = sqlProps.getProperty(key).trim();
            counter.l2 = 0L;
            PerfTask task = new PerfTask("testSql_" + key, numOfWarmups, numOfSelects, ctx -> {
                List<Map<String, Object>> rows = jdbc.queryForList(sql, sqlParam);
                if (ctx.runType != PerfTask.RunType.WARMUP) {
                    counter.l = rows.size();
                }
            });
            task.timeit();

            task.addExtraField("Query", sql);
            task.addExtraField("Result Rows Fetched", counter.l);
            task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
            writer.writeTask(task);
        }
    }

    private void closeWithoutException(Connection conn) {
        try {
            conn.close();
        } catch (Exception e) {
            LOG.warn("Failed to close conn" + conn, e);
        }
    }
}


