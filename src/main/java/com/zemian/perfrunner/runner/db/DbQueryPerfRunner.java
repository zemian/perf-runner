package com.zemian.perfrunner.runner.db;

import com.zemian.perfrunner.DCounter;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Execute query from files.
 */
@Profile("db-runner")
@Component
@Order(202)
public class DbQueryPerfRunner extends PerfRunner {
    private static final Logger LOG = getLogger(DbQueryPerfRunner.class);

    @Autowired
    private JdbcTemplate jdbc;

    @Value("${perfrunner.DbQueryPerfRunner.queryFileList:classpath:DbQueryPerfRunner-sql.xml}")
    private Resource sqlPropsRes;
    private Properties sqlProps;

    @Value("${perfrunner.DbQueryPerfRunner.numOfWarmupRuns:1000}")
    private int numOfWarmupRuns;

    @Value("${perfrunner.DbQueryPerfRunner.numOfRuns:5000}")
    private int numOfRuns;

    @Override
    public void init() throws Exception {
        sqlProps = new Properties();
        try (InputStream in = sqlPropsRes.getInputStream()) {
            sqlProps.loadFromXML(in);
        }
    }

    @Override
    public void run() throws Exception {
        testDriverInfo();
        testQueries();
    }

    @SuppressWarnings("unchecked")
    public void testDriverInfo() {
        PerfTask task = new PerfTask("testDriverInfo", "Collect database info", ctx -> {
            jdbc.execute((ConnectionCallback) conn -> {
                DatabaseMetaData md = conn.getMetaData();
                ctx.task.addExtraField("URL", md.getURL());
                ctx.task.addExtraField("DatabaseProductName", md.getDatabaseProductName());
                ctx.task.addExtraField("DatabaseProductVersion", md.getDatabaseProductVersion());
                ctx.task.addExtraField("DriverName", md.getDriverName());
                ctx.task.addExtraField("DriverVersion", md.getDriverVersion());
                return null;
            });
        });
        task.timeit();
        writer.writeTask(task);
    }

    /*
    This query and use metadata to populate map key with column names. (Not your fastest query!)
     */
    public void testQueries() {
        LOG.debug("Processing sqlProps size={}", sqlProps.size());
        List<String> names = new ArrayList<>(sqlProps.stringPropertyNames());
        Collections.sort(names);
        for (String name : names) {
            String sql = sqlProps.getProperty(name).trim();
            testQuery(name, sql);
        }
    }

    public void testQuery(String name, String sql) {
        // Query without introspecting column names per each row! It fetch list of columns per record.
        DCounter counter = new DCounter();
        PerfTask task = new PerfTask("testQuery_" + name, numOfWarmupRuns, numOfRuns, ctx -> {
            jdbc.query(sql, rs -> {
                if (counter.i == 0)
                    counter.i = rs.getMetaData().getColumnCount();
                List<List<Object>> rows = new ArrayList<>();
                while (rs.next()) {
                    List<Object> row = new ArrayList<>();
                    for (int i = 1; i <= counter.i; i++) {
                        row.add(rs.getObject(i));
                    }
                    rows.add(row);
                }

                // Record the last rows size
                counter.l = rows.size();
                return null;
            });
        });
        task.timeit();

        task.addExtraField("Query", sql);
        task.addExtraField("Query Result Size", counter.l);
        task.addExtraField("Rate", task.getAvgRunTime(), "ms/query");
        writer.writeTask(task);
    }
}


