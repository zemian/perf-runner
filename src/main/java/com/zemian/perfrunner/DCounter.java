package com.zemian.perfrunner;

/**
 * A data wrapper class for assisting perf task operation.
 */
public class DCounter {
    public double d;
    public double d2;
    public double d3;
    public long l;
    public long l2;
    public long l3;
    public Object o;
    public Object o2;
    public Object o3;
    public int i;
    public int i2;
    public int i3;

    public void emptyMethod() {
    }

    @SuppressWarnings("unchecked")
    public <T> T getO() {
        return (T)o;
    }
    @SuppressWarnings("unchecked")
    public <T> T getO2() {
        return (T)o2;
    }
    @SuppressWarnings("unchecked")
    public <T> T getO3() {
        return (T)o3;
    }
}
