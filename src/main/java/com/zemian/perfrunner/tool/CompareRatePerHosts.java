package com.zemian.perfrunner.tool;

import com.zemian.perfrunner.PerfTask;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
public class CompareRatePerHosts {
    private static final Logger LOG = LoggerFactory.getLogger(CompareRatePerHosts.class);

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext spring = SpringApplication.run(CompareRatePerHosts.class, args);
        CompareRatePerHosts service = spring.getBean(CompareRatePerHosts.class);
        service.run(args);
        spring.close();
    }

    @Autowired
    private JdbcTemplate jdbc;

    public void run(String[] args) throws Exception {

        Map<String, Map<String, PerfTask>> tasksByNameByHost = new TreeMap<>();

        // Get all the unique hosts with their latest report id
        String sql = "SELECT max(a.id) report_id, c.value hostname\n" +
                "FROM perf_report a\n" +
                " LEFT JOIN perf_taskrun b ON a.id = b.report_id\n" +
                " LEFT JOIN perf_taskextra c ON b.id = c.taskrun_id\n" +
                " WHERE b.name = 'systemInfo' AND c.name = 'system.hostname'\n" +
                " GROUP BY c.value\n" +
                " ORDER BY c.value\n";
        List<Map<String, Object>> reportRows = jdbc.queryForList(sql);

        // Get taskrun for each host report
        for (Map<String, Object> reportRow : reportRows) {
            Integer reportId = (Integer) reportRow.get("report_id");
            String hostname = (String) reportRow.get("hostname");

            Map<String, PerfTask> tasksByName = tasksByNameByHost.get(hostname);
            if (tasksByName == null) {
                tasksByName = new TreeMap<>();
                tasksByNameByHost.put(hostname, tasksByName);
            }

            sql = "SELECT b.* FROM perf_report a\n" +
                    " LEFT JOIN perf_taskrun b ON a.id = b.report_id\n" +
                    " WHERE a.id = ?\n" +
                    " ORDER BY b.name";
            List<Map<String, Object>> taskrunRows = jdbc.queryForList(sql, reportId);

            for (Map<String, Object> taskrunRow : taskrunRows) {
                String taskName = (String) taskrunRow.get("name");
                String taskDesc = (String) taskrunRow.get("description");

                // The task here is just used to hold data, not for running.
                PerfTask task = new PerfTask(taskName, taskDesc, ctx -> {});
                task.addExtraField("_rate", taskrunRow.get("rate"));
                task.addExtraField("_runnerId", taskrunRow.get("runner_id"));
                task.addExtraField("_taskrunId", taskrunRow.get("id"));

                Integer taskrunId = (Integer) taskrunRow.get("id");
                sql = "SELECT * FROM perf_taskextra WHERE taskrun_id = ? ORDER BY name";
                List<Map<String, Object>> extraRows = jdbc.queryForList(sql, taskrunId);
                for (Map<String, Object> extraRow : extraRows) {
                    String extraName = (String) extraRow.get("name");
                    Object extraVal = extraRow.get("value");
                    String unit = (String) extraRow.get("unit");
                    task.addExtraField(extraName, extraVal, unit);
                }
                tasksByName.put(task.getName(), task);
            }
        }

        List<List<String>> csvLines = new ArrayList<>();

        // Fill the header line
        List<String> lineItems = new ArrayList<>();
        csvLines.add(lineItems);
        lineItems.add("Task");
        lineItems.add("Unit");
        Set<String> hosts = tasksByNameByHost.keySet();
        for (String host : hosts) {
            lineItems.add(host);
        }
        LOG.info("Found hosts={}, size={}", hosts, hosts.size());

        // First the task names set from a host that has most count
        List<Collection<PerfTask>> taskNamesA = tasksByNameByHost.values().stream().map(a -> a.values()).collect(Collectors.toList());
        Collection<PerfTask> taskNamesB = taskNamesA.stream().sorted((a, b) -> Integer.compare(b.size(), a.size())).findFirst().get();
        List<String> taskNames = taskNamesB.stream().map(a -> a.getName()).collect(Collectors.toList());

        //Set<String> taskNames
        LOG.info("Found taskNames size={}", taskNames.size());
        for (String taskName : taskNames) {
            lineItems = new ArrayList<>();
            csvLines.add(lineItems);
            lineItems.add(taskName);

            for (String hostname : hosts) {
                Map<String, PerfTask> tasksByName = tasksByNameByHost.get(hostname);
                PerfTask task = tasksByName.get(taskName);
                if (task == null)
                    continue;

                boolean added = addExtraRateLineItem(lineItems, task, "testTightLoop", "loops/ms") ||
                        addExtraRateLineItem(lineItems, task, "testCol", "items/ms") ||
                        addExtraRateLineItem(lineItems, task, "testFilesDelete", "files/sec") ||
                        addExtraRateLineItem(lineItems, task, "testFiles", "bytes/ms") ||
                        addExtraRateLineItem(lineItems, task, "testSql", "records/ms") ||
                        addExtraRateLineItem(lineItems, task, "testQuery", "records/ms");
                if (!added) {
                    if (lineItems.size() < 2)
                        lineItems.add("runs/ms");

                    String val = task.getExtraField("_rate").getFormattedValue();
                    lineItems.add(val);
                }
            }
        }

        File file = new File("target/perf-rates-by-hosts.csv");
        try (FileWriter writer = new FileWriter(file)) {
            for (List<String> csvLine : csvLines) {
                writer.write(StringUtils.join(csvLine, ",") + "\n");
            }
        }
        LOG.info("CSV file created={}", file);
    }

    private boolean addExtraRateLineItem(List<String> lineItems, PerfTask task,
                                         String wantedTaskName, String wantedUnit) {
        if (wantedTaskName != null &&
                task.getName().startsWith(wantedTaskName)) {

            if (lineItems.size() < 2)
                lineItems.add(wantedUnit);

            Optional<PerfTask.TaskExtra> found = task.getExtraFields().stream().filter(
                    a -> "Rate".equals(a.name) && wantedUnit.equals(a.unit)).findFirst();

            if (found.isPresent()) {
                PerfTask.TaskExtra extra = found.get();
                lineItems.add(extra.getFormattedValue());
            } else {
                lineItems.add("");
            }
            return true;
        }
        return false;
    }
}
