package com.zemian.perfrunner.writer;

import com.zemian.perfrunner.PerfWriter;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class AbstractFileWriter implements PerfWriter {
    protected Logger LOG = getLogger(getClass());

    public static final String SYSINFO_VAR_PATTERN = "%(osinfo)";
    public static final String HOSTNAME_VAR_PATTERN = "%(hostname)";
    public static final String DATETIME_VAR_PATTERN = "%(datetime)";

    @Value("${perfrunner.AsciidocWriter.reportDirectory:target}")
    protected File reportDirectory;

    @Value("${perfrunner.AsciidocWriter.reportFileName:}")
    protected String reportFileName;

    protected PrintWriter writer;

    public String getFullReportFileName() {
        return reportDirectory.getPath() + "/" + reportFileName;
    }

    protected void println(String line) {
        if (line == null)
            line = "";

        LOG.info(line);

        writer.write(line + "\n");
        writer.flush();
    }

    @Override
    public void init() throws Exception {
        // Ensure directory exist
        if (!reportDirectory.exists()) {
            LOG.info("Making reportDirectory={}", reportDirectory);
            reportDirectory.mkdirs();
        }

        // Replace var pattern if found.
        if ("".equals(reportFileName)) {
            reportFileName = getDefaultReportFileName();
        }

        // Expand vars
        if (reportFileName.indexOf(SYSINFO_VAR_PATTERN) >= 0) {
            String name = System.getProperty("os.name").replaceAll(" ", "");
            String arch = System.getProperty("os.arch").replaceAll(" ", "");
            String ver = System.getProperty("os.version").replaceAll(" ", "");
            String osinfo = name + "-" + arch + "-" + ver;
            reportFileName = reportFileName.replaceAll(Pattern.quote(SYSINFO_VAR_PATTERN), osinfo);
        }
        if (reportFileName.indexOf(HOSTNAME_VAR_PATTERN) >= 0) {
            String name = InetAddress.getLocalHost().getHostName();
            reportFileName = reportFileName.replaceAll(Pattern.quote(HOSTNAME_VAR_PATTERN), name);
        }
        if (reportFileName.indexOf(DATETIME_VAR_PATTERN) >= 0) {
            SimpleDateFormat df = new SimpleDateFormat("yyMMdd-HHmmss");
            String dt = df.format(new Date());
            reportFileName = reportFileName.replaceAll(Pattern.quote(DATETIME_VAR_PATTERN), dt);
        }

        LOG.info("Generating perf report on FILE={}", getFullReportFileName());
        FileOutputStream fstream = new FileOutputStream(getFullReportFileName());
        writer = new PrintWriter(new OutputStreamWriter(fstream, Charset.forName("UTF-8")));
    }

    protected abstract String getDefaultReportFileName();

    @Override
    public void destroy() throws Exception {
        writer.close();
    }
}
