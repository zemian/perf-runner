package com.zemian.perfrunner.writer;

import com.zemian.perfrunner.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

@Profile("db-writer")
@Component
public class JdbcWriter implements PerfWriter {
    private static final Logger LOG = getLogger(PerfRunnerService.class);

    @Autowired
    private JdbcTemplate jdbc;

    private int reportId;
    private int runnerId;

    @Override
    public void init() throws Exception {
    }

    @Override
    public void destroy() throws Exception {
    }

    private String getHostname() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            throw new AppException("Failed to get hostname");
        }
    }

    @Override
    public void writeReportBefore(long startTime) {
        String reportName = "perf-report_" + getHostname();
        String sql = "INSERT INTO perf_report(name, start) VALUES(?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(conn -> {
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, reportName);
            ps.setTimestamp(2, new Timestamp(startTime));
            return ps;
        }, keyHolder);
        reportId = (Integer)keyHolder.getKeys().get("id");
        LOG.debug("Inserted reportId={}, reportName={}", reportId, reportName);
    }

    @Override
    public void writeReportAfter(long startTime, long stopTime, int runnerCount) {
        String sql = "UPDATE perf_report SET stop = ? WHERE id = ?";
        jdbc.update(sql, new Timestamp(stopTime), reportId);
        LOG.debug("End reportId={}", reportId);
    }

    @Override
    public void writeRunnerBefore(PerfRunner runner) {
        String name = runner.getClass().getSimpleName();
        String querySql = "SELECT id FROM perf_runner WHERE name = ?";
        List<Map<String, Object>> list = jdbc.queryForList(querySql, name);

        if (list.size() == 0) {
            String sql = "INSERT INTO perf_runner(name, description) VALUES(?, ?)";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbc.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, name);
                ps.setString(2, runner.getClass().getName());
                return ps;
            }, keyHolder);
            runnerId = (Integer)keyHolder.getKeys().get("id");
            LOG.debug("Inserted runnerId={}", runnerId);
        } else {
            runnerId = (Integer)list.get(0).get("id");
            LOG.debug("Using runnerId={}", runnerId);
        }
    }

    @Override
    public void writeRunnerAfter(PerfRunner runner) {
        runnerId = -1;
    }

    @Override
    public void writeTask(PerfTask task) {
        List<Object> params = new ArrayList<>();
        params.add(reportId);
        params.add(runnerId);
        params.add(task.getName());

        params.add(task.getDescription());
        params.add(task.getNumOfWarmupRuns());
        params.add(task.getNumOfRuns());

        params.add(task.getRunType().name());
        params.add(task.isCompleted());
        params.add(task.isError());

        params.add(task.getError());

        if (task.isCompleted()) {
            params.add(Timestamp.valueOf(task.getStart()));
            params.add(Timestamp.valueOf(task.getStop()));
        } else {
            params.add(null); // start
            params.add(null); // stop
        }
        if (task.isCompleted() && task.getTotalRuns() > 0) {
            params.add(task.getTotalRuns());
            params.add(task.getMaxRunTime());
            params.add(task.getMinRunTime());

            params.add(task.getAvgRunTime());
            params.add(task.getRate());
            params.add(task.getTotalRunTime());
        } else {
            params.add(null); // total_runs
            params.add(null); // max_run
            params.add(null); // min_run

            params.add(null); // avg_run
            params.add(null); // rate
            params.add(null); // total_run_time
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(conn -> {
            String sql = "INSERT INTO perf_taskrun(" +
                    "report_id, runner_id, name, " +
                    "description, warmup_runs, num_runs, " +
                    "run_type, iscomplete, iserror, " +
                    "error, start, stop, " +
                    "total_runs, max_run, min_run, " +
                    "avg_run, rate, total_run_time) VALUES(" +
                    "?, ?, ?, " +
                    "?, ?, ?, " +
                    "?, ?, ?, " +
                    "?, ?, ?, " +
                    "?, ?, ?, " +
                    "?, ?, ?) ";
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for (Object param : params) {
                ps.setObject(i++, param);
            }
            return ps;
        }, keyHolder);
        Integer taskRunId = (Integer) keyHolder.getKeys().get("id");
        LOG.debug("Inserted PerfTask with taskId={}, taskName={}", taskRunId, task.getName());

        List<PerfTask.TaskExtra> extraFields = task.getExtraFields();
        if (extraFields.size() > 0) {
            String sql = "INSERT INTO perf_taskextra( " +
                    "taskrun_id, name, value, " +
                    "type, unit) VALUES( " +
                    "?, ?, ?, " +
                    "?, ?) ";
            for (PerfTask.TaskExtra extra : extraFields) {
                jdbc.update(sql,
                        taskRunId, extra.name, extra.val,
                        extra.type, extra.unit);
            }
            LOG.debug("Inserted task extra map size={}", extraFields.size());
        }
    }
}
