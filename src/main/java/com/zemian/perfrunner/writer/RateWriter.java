package com.zemian.perfrunner.writer;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.zemian.perfrunner.Util.DF_3;

/**
 * A simple writer that only prints the rate of each task.
 */
@Profile({"default", "rate-writer"})
@Component
public class RateWriter extends AbstractFileWriter {
    @Override
    public void writeReportBefore(long startTime) {

    }

    @Override
    public void writeReportAfter(long startTime, long stopTime, int runnerCount) {

    }

    @Override
    public void writeRunnerBefore(PerfRunner runner) {

    }

    @Override
    public void writeRunnerAfter(PerfRunner runner) {

    }

    @Override
    public String getDefaultReportFileName() {
        return "perf-report-rate_%(hostname)_%(datetime).txt";
    }

    @Override
    public void writeTask(PerfTask task) {
        String taskName = task.getName();

        // First to see if "Rate" exists in extra fields, print first one found.
        Optional<PerfTask.TaskExtra> found = task.getExtraFields().stream().filter(
                a -> "Rate".equals(a.name)).findFirst();

        if (found.isPresent()) {
            PerfTask.TaskExtra extra = found.get();
            println(taskName + ": " + extra.getFormattedValue() + " " + extra.unit);

        } else {
            // If not found, just print the task run rate
            println(taskName + ": " + DF_3.format(task.getRate()) + " runs/ms");
        }
    }
}
