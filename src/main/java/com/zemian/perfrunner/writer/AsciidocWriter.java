package com.zemian.perfrunner.writer;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.PerfTask;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Date;
import java.util.Map;

/**
 * Write report to LOG and FILE (if writeFile flag is on).
 */
@Profile("adoc-writer")
@Component
public class AsciidocWriter extends AbstractFileWriter {

    @Override
    public String getDefaultReportFileName() {
        return "perf-report-details_%(hostname)_%(datetime).adoc";
    }

    @Override
    public void writeReportBefore(long startTime) {
        println("= Performance Report");
        println(":file: " + getFullReportFileName());
        println(":date: " + new Date(startTime));
        println("");
    }

    @Override
    public void writeReportAfter(long startTime, long stopTime, int runnerCount) {
        println("== Report Summary");
        println("\nReport Start Date: " + new Date(startTime));
        println("\nReport Stop Date: " + new Date(stopTime));
        println("\nDuration: " + Duration.ofMillis(stopTime - startTime));
        println("\nNumber of Runners: " + runnerCount);
    }

    @Override
    public void writeRunnerBefore(PerfRunner runner) {
        println("== " + runner.getClass().getSimpleName());
        println("");
    }

    @Override
    public void writeRunnerAfter(PerfRunner runner) {
        println("");
    }

    public void writeTaskItem(String name, Object value) {
        println("  " + name + ": " + value);
    }

    /*
    This will auto invoke writeTaskBefore and writeTaskAfter!
     */
    @Override
    public void writeTask(PerfTask task) {
        println("----");
        println("TaskResult:");

        for (Map.Entry<String, String> entry : task.getTaskResults().entrySet()) {
            writeTaskItem(entry.getKey(), entry.getValue());
        }

        if (task.getExtraFields().size() > 0) {
            println("Extras:");
            for (PerfTask.TaskExtra taskExtra : task.getExtraFields()) {
                writeTaskItem(taskExtra.getFormattedName(), taskExtra.getFormattedValue());
            }
        }

        println("----");
        println("");
    }
}
