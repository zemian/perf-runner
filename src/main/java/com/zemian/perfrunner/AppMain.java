package com.zemian.perfrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * A main entry into the application and provide Spring Config settings.
 */
@SpringBootApplication
public class AppMain {
	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext spring = SpringApplication.run(AppMain.class, args);
		PerfRunnerService service = spring.getBean(PerfRunnerService.class);
		service.run(args);
		spring.close();
	}
}
