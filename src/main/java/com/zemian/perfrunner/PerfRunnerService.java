package com.zemian.perfrunner;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

@Component
public class PerfRunnerService {
    private static final Logger LOG = getLogger(PerfRunnerService.class);

    @Autowired
    private List<PerfRunner> perfRunners;

    @Value("${includeRunners:#{T(java.util.Collections).emptySet()}}")
    private Set<String> includeRunners = new LinkedHashSet<>();

    @Value("${perfrunner.PerfRunnerService.excludeRunners:#{T(java.util.Collections).emptySet()}")
    private Set<String> excludeRunners = new HashSet<>();

    @Autowired
    protected PerfWriter writer;

    private RunnerSharedData runnersSharedData = new RunnerSharedData();

    @PostConstruct
    public void init() throws Exception {
        writer.init();

        if (includeRunners.size() > 0) {
            LOG.debug("Using includeRunners={}, size={}", includeRunners, includeRunners.size());
        }
        if (excludeRunners.size() > 0) {
            LOG.debug("Using excludeRunners={}, size={}", excludeRunners, excludeRunners.size());
        }
    }

    @PreDestroy
    public void destroy() throws Exception {
        writer.destroy();
    }

    public void run(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        int runnerCount = 0;
        writer.writeReportBefore(start);

        List<PerfRunner> runners;
        if (includeRunners.size() > 0) {
            runners = perfRunners.stream().
                    filter(a -> includeRunners.contains(a.getClass().getSimpleName()))
                    .collect(Collectors.toList());
        } else {
            runners = perfRunners;
        }

        // Init runners
        for (PerfRunner runner : runners) {
            runner.init(runnersSharedData);
        }

        for (PerfRunner perfRunner : runners) {
            String runnerName = perfRunner.getClass().getSimpleName();
            LOG.debug("Checking runnerName={}", runnerName);
            if (excludeRunners.contains(runnerName)) {
                LOG.debug("Excluding {} due to match in excludeRunners set.", runnerName);
                continue;
            }

            try {
                writer.writeRunnerBefore(perfRunner);
                perfRunner.run();
                runnerCount++;
                writer.writeRunnerAfter(perfRunner);
            } catch (Throwable t) {
                LOG.error("Failed to execute " + runnerName, t);
            }
        }

        // Destroy runners
        for (PerfRunner runner : runners) {
            runner.destroy();
        }

        LOG.debug("Done with all runners.");

        long stop = System.currentTimeMillis();
        writer.writeReportAfter(start, stop, runnerCount);
    }
}
