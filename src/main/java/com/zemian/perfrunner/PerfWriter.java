package com.zemian.perfrunner;

public interface PerfWriter {
    void init() throws Exception;
    void destroy() throws Exception;
    void writeReportBefore(long startTime);
    void writeReportAfter(long startTime, long stopTime, int runnerCount);
    void writeRunnerBefore(PerfRunner runner);
    void writeRunnerAfter(PerfRunner runner);
    void writeTask(PerfTask task);
}
