package com.zemian.perfrunner;

import java.text.DecimalFormat;

public class Util {
    public static final DecimalFormat DF = new DecimalFormat("#,###.#");
    public static final DecimalFormat DF_3 = new DecimalFormat("#,###.###");
}
