package com.zemian.perfrunner;

import org.slf4j.Logger;
import org.springframework.util.Assert;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.zemian.perfrunner.PerfTask.RunType.*;
import static com.zemian.perfrunner.Util.DF;
import static com.zemian.perfrunner.Util.DF_3;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Given a task, we will repeat it #numOfRuns times and capture the time metrics.
 */
public class PerfTask {
    private static final Logger LOG = getLogger(PerfTask.class);

    public enum RunType { NONE, WARMUP, TIMEIT, TIMEIT_FOR_MILLIS }

    public static class RunContext {
        public RunType runType;
        public int runNum;
        public Instant start;
        public PerfTask task;
    }

    public interface Task {
        void execute(RunContext runContext) throws Exception;
    }

    public class TaskExtra {
        public String name;
        public Object val;
        public String type;
        public String unit;

        public String getFormattedName() {
            String result = name;
            if (unit != null)
                result += " (" + unit + ")";
            return result;
        }

        public String getFormattedValue() {
            if ("D".equals(type)) {
                return DF_3.format(val);
            } else if ("N".equals(type)) {
                return DF.format(val);
            } else {
                return val.toString();
            }
        }
    }

    private String name;
    private String description;
    private int numOfWarmupRuns;
    private int numOfRuns;
    private Task task;
    private LocalDateTime start;
    private LocalDateTime stop;
    private boolean isCompleted;
    private boolean isError;
    private String error;
    private List<Duration> taskRunTimes = new ArrayList<>();
    private List<TaskExtra> extraFields = new ArrayList<>();
    private RunType runType = NONE;

    /* Default to 0 warmsup and 0 runs. */
    public PerfTask(String name, String description, Task task) {
        this(name, description, 0, 1, task);
    }

    public PerfTask(String name, int numOfWarmupRuns, int numOfRuns, Task task) {
        this(name, null, numOfWarmupRuns, numOfRuns, task);
    }

    public PerfTask(String name, String description, int numOfWarmupRuns, int numOfRuns, Task task) {
        this.name = name;
        this.description = description;
        this.numOfWarmupRuns = numOfWarmupRuns;
        this.numOfRuns = numOfRuns;
        this.task = task;
    }

    public RunType getRunType() {
        return runType;
    }

    public String getError() {
        return error;
    }

    public boolean isError() {
        return isError;
    }

    public String getDescription() {
        return description;
    }

    public void addExtraField(String name, Object val) {
        addExtraField(name, val, null);
    }

    public void addExtraField(String name, Object val, String unit) {
        String type = getShortTypeName(val);
        TaskExtra a = new TaskExtra();
        a.name = name;
        a.val = val;
        a.unit = unit;
        a.type = type;
        extraFields.add(a);
    }

    private String getShortTypeName(Object val) {
        String result = "O";
        Class<?> cls = val.getClass();
        if (String.class.equals(cls)) {
            result = "S";
        } else if (Double.class.equals(cls)) {
            result = "D";
        } else if (Long.class.equals(cls) || Integer.class.equals(cls)) {
            result = "N";
        } else if (Boolean.class.equals(cls)) {
            result = "B";
        } else if (Date.class.equals(cls) || LocalDateTime.class.equals(cls)) {
            result = "T";
        }
        return result;
    }

    public String getName() {
        return name;
    }

    public int getNumOfWarmupRuns() {
        return numOfWarmupRuns;
    }

    public int getNumOfRuns() {
        return numOfRuns;
    }

    public LocalDateTime getStart() {
        Assert.notNull(start, "You have not yet start the run task.");
        return start;
    }

    public LocalDateTime getStop() {
        Assert.notNull(stop, "You have not yet stop the run task.");
        return stop;
    }

    public List<TaskExtra> getExtraFields() {
        return extraFields;
    }

    public boolean existsExtraField(String name) {
        return extraFields.stream().anyMatch(a -> name.equals(a.name));
    }

    public TaskExtra getExtraField(String name) {
        return extraFields.stream().filter(a -> name.equals(a.name)).findFirst().get();
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    private void execteRun(RunContext ctx) {
        Assert.isNull(stop, "You have already ran this task.");
        try {
            ctx.start = Instant.now();
            task.execute(ctx);
            taskRunTimes.add(Duration.between(ctx.start, Instant.now()));
        } catch (Exception e) {
            isError = true;
            error = e.getMessage();
            LOG.error("Task failed to execute.", e);
        }
    }

    private void warmUpRun() {
        if (numOfWarmupRuns > 0) {
            for (int i = 0; i < numOfWarmupRuns; i++) {
                RunContext ctx = new RunContext();
                ctx.runNum = i;
                ctx.runType = RunType.WARMUP;
                ctx.task = this;
                execteRun(ctx);
            }
        }
        // Clearn the times recorded during warmup runs.
        taskRunTimes.clear();
    }

    public void timeit() {
        warmUpRun();

        // Start
        start = LocalDateTime.now();
        runType = TIMEIT;

        // Execute
        for (int i = 0; i < numOfRuns; i++) {
            RunContext ctx = new RunContext();
            ctx.runNum = i;
            ctx.runType = RunType.TIMEIT;
            ctx.task = this;
            execteRun(ctx);
        }

        // Stop
        stop = LocalDateTime.now();
        isCompleted = true;
    }

    public void timeitForMillis(long period) {
        warmUpRun();

        // Start
        start = LocalDateTime.now();
        runType = TIMEIT_FOR_MILLIS;

        CountDownLatch lastTask = new CountDownLatch(1);
        AtomicBoolean cont = new AtomicBoolean(true);
        Thread t = new Thread(() -> {
            int counter = 1;
            while (cont.get()) {
                RunContext ctx = new RunContext();
                ctx.runNum = counter++;
                ctx.runType = RunType.TIMEIT_FOR_MILLIS;
                ctx.task = this;
                execteRun(ctx);
            }
            lastTask.countDown();
        });

        // Execute
        t.start();

        // Wait for it
        try {
            sleepFully(period);
            cont.set(false);
            lastTask.await();
        } catch (InterruptedException e) {
            throw new AppException("Unable to wait for task.", e);
        }

        // Stop
        stop = LocalDateTime.now();
        isCompleted = true;
    }

    /*
    This ensure to sleep min of given millis.
     */
    private void sleepFully(long millis) {
        long t1 = System.currentTimeMillis();
        while (System.currentTimeMillis() - t1 < millis) {

            try {
                long delay = millis - (System.currentTimeMillis() - t1);
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // Ingore
            }
        }
    }

    public List<Duration> getTaskRunTimes() {
        Assert.notNull(stop, "You have not yet run task.");
        return taskRunTimes;
    }

    public int getTotalRuns() {
        Assert.notNull(stop, "You have not yet run task.");
        return taskRunTimes.size();
    }

    /* Sum all runs duration in millis */
    public long getTotalRunTime() {
        Assert.notNull(stop, "You have not yet run task.");
        if (runType == TIMEIT) {
            return taskRunTimes.stream().mapToLong(a -> a.toMillis()).sum();
        } else if (runType == TIMEIT_FOR_MILLIS) {
            return Duration.between(start, stop).toMillis();
        } else {
            throw new AppException("Invalid runType=" + runType);
        }
    }

    /* Avg of each run in millis */
    public double getAvgRunTime() {
        Assert.notNull(stop, "You have not yet run task.");
        return taskRunTimes.stream().mapToLong(a -> a.toMillis()).average().getAsDouble();
    }

    /* Max run time in millis */
    public long getMaxRunTime() {
        Assert.isTrue(runType != NONE, "You have not yet run task.");
        return taskRunTimes.stream().mapToLong(a -> a.toMillis()).max().getAsLong();
    }

    /* Min run time in millis */
    public long getMinRunTime() {
        Assert.notNull(stop, "You have not yet run task.");
        return taskRunTimes.stream().mapToLong(a -> a.toMillis()).min().getAsLong();
    }

    public double getRate() {
        Assert.notNull(stop, "You have not yet run task.");
        return ((double)getTotalRuns()) / getTotalRunTime();
    }

    /*
    Format task field value into a nice string. These are the non-extra fields.
     */
    private String formatValue(Object val) {
        if (val instanceof Double) {
            return DF_3.format(val);
        } else if (val instanceof Long || val instanceof Integer) {
            return DF.format(val);
        } else {
            return val.toString();
        }
    }

    public Map<String, String> getTaskResults() {
        Map<String, String> result = new LinkedHashMap<>();
        result.put("Name", getName());
        if (getDescription() != null)
            result.put("Description", getDescription());

        if (isCompleted()) {
            result.put("Start", formatValue(getStart()));
            result.put("Stop", formatValue(getStop()));
            if (isError()) {
                result.put("ERROR", getError());
            } else if (getTotalRuns() > 0) {
                result.put("Total Runs", formatValue(getTotalRuns()));
                result.put("Total Times (ms)", formatValue(getTotalRunTime()));
                result.put("Max Run (ms)", formatValue(getMaxRunTime()));
                result.put("Min Run (ms)", formatValue(getMinRunTime()));
                result.put("Avg Run (ms)", formatValue(getAvgRunTime()));
                result.put("Rate (runs/ms)", formatValue(getRate()));
            }
        } else {
            result.put("ERROR", "TASK HAS NOT YET RUN.");
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PerfTask{\n");
        for (Map.Entry<String, String> e : getTaskResults().entrySet()) {
            String line = "  " + e.getKey() + ": " + e.getValue();
            sb.append(line + "\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
