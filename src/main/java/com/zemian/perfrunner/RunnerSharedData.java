package com.zemian.perfrunner;

import java.util.HashMap;
import java.util.Map;

public class RunnerSharedData {
    private Map<String, Object> map = new HashMap<>();

    public void add(String name, Object val) {
        map.put(name, val);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String name) {
        return (T) map.get(name);
    }

    public double compareRate(String name, double rate) {
        double baseRate = get(name);
        double compareRate = ((rate - baseRate) / baseRate) * 100.0;
        return compareRate;
    }
}
