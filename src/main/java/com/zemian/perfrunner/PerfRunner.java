package com.zemian.perfrunner;

import org.springframework.beans.factory.annotation.Autowired;

import java.text.DecimalFormat;
import java.util.Map;

public abstract class PerfRunner {
    protected RunnerSharedData runnersSharedData;

    public abstract void run() throws Exception;
    public void init(RunnerSharedData runnersSharedData) throws Exception {
        this.runnersSharedData = runnersSharedData;
        init();
    }
    public void init() throws Exception {}
    public void destroy() throws Exception {}

    @Autowired
    protected PerfWriter writer;

    public PerfWriter getWriter() {
        return writer;
    }
}
