package com.zemian.perfrunner;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PerfWriterTest {
    @Test
    public void testVarReplace() {
        String name = "perf-report_${osinfo}";
        name = name.replaceAll(Pattern.quote("${osinfo}"), "foo");
        assertThat(name, is("perf-report_foo"));
    }
}
