package com.zemian.perfrunner;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class PerfTaskTest {

    @Test
    public void testNotRunTask() {
        PerfTask task = new PerfTask("testNotRunTask", "Just a test", 10, 100, ctx -> {});
        assertThat(task.getName(), is("testNotRunTask"));
        assertThat(task.getDescription(), is("Just a test"));
        assertThat(task.getNumOfWarmupRuns(), is(10));
        assertThat(task.getNumOfRuns(), is(100));
        try {
            task.getStart();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getStop();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getMaxRunTime();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getMinRunTime();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getAvgRunTime();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getTotalRuns();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getTotalRunTime();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }
        try {
            task.getRate();
            Assert.fail("You should not geting value before timeit().");
        } catch (Exception e) { }

        Map<String, String> results = task.getTaskResults();
        assertThat(results, hasEntry("Name", "testNotRunTask"));
        assertThat(results, hasEntry("ERROR", "TASK HAS NOT YET RUN."));

        // ToString of unrun task
        String out = task.toString();
        assertThat(out, containsString("PerfTask{"));
        assertThat(out, containsString("Name: "));
        assertThat(out, containsString("Description: "));
        assertThat(out, containsString("ERROR: TASK HAS NOT YET RUN."));
        assertThat(out, containsString("}"));
    }

    @Test
    public void testToString() {
        PerfTask task = new PerfTask("testToString", "Just a test", 0, 1, ctx -> {});
        task.timeit();
        String out = task.toString();
        assertThat(out, containsString("PerfTask{"));
        assertThat(out, containsString("Name: "));
        assertThat(out, containsString("Description: "));
        assertThat(out, containsString("Start: "));
        assertThat(out, containsString("Stop: "));
        assertThat(out, containsString("Total Runs: "));
        assertThat(out, containsString("Total Times (ms): "));
        assertThat(out, containsString("Max Run (ms): "));
        assertThat(out, containsString("Min Run (ms): "));
        assertThat(out, containsString("Avg Run (ms): "));
        assertThat(out, containsString("}"));
    }

    @Test
    public void testTimeit() {
        LocalDateTime t1 = LocalDateTime.now();
        PerfTask task = new PerfTask("testTimeit", "Just a test", 10, 100, ctx -> { });
        task.timeit();
        assertThat(task.getName(), is("testTimeit"));
        assertThat(task.getDescription(), is("Just a test"));
        assertThat(task.getRunType(), is(PerfTask.RunType.TIMEIT));
        assertThat(task.getStop(), greaterThanOrEqualTo(task.getStart()));
        assertThat(task.getStop(), greaterThanOrEqualTo(t1));
        assertThat(task.getNumOfWarmupRuns(), is(10));
        assertThat(task.getNumOfRuns(), is(100));
        assertThat(task.getTotalRuns(), greaterThanOrEqualTo(100));
        assertThat(task.getTotalRunTime(), greaterThanOrEqualTo(0L));
        assertThat(task.getMaxRunTime(), greaterThanOrEqualTo(0L));
        assertThat(task.getMinRunTime(), greaterThanOrEqualTo(0L));
        assertThat(task.getAvgRunTime(), greaterThanOrEqualTo(0.0));
        assertThat(task.getRate(), greaterThanOrEqualTo(1_000.0));

        Map<String, String> results = task.getTaskResults();
        assertThat(results, hasEntry("Name", "testTimeit"));
        assertThat(results, hasKey("Rate (runs/ms)"));
    }

    @Test
    public void testExtraFields() {
        PerfTask task = new PerfTask("testTimeit", "Just a test", 10, 100, ctx -> { });
        assertThat(task.getExtraFields().size(), is(0));

        task.addExtraField("foo", "bar");
        task.addExtraField("foo_int", 998877);
        task.addExtraField("foo_dval", 332211.234567);

        assertThat(task.getExtraFields().size(), is(3));

        task.timeit();
        assertThat(task.getExtraFields().size(), is(3));
        assertThat(task.getRate(), greaterThanOrEqualTo(1_000.0));

        assertThat(task.getExtraField("foo").val, is("bar"));
        assertThat(task.getExtraField("foo_int").val, is(998877));
        assertThat(task.getExtraField("foo_dval").val, is(332211.234567));
        assertThat(task.getExtraField("foo_int").getFormattedValue(), is("998,877"));
        assertThat(task.getExtraField("foo_dval").getFormattedValue(), is("332,211.235"));

        task.addExtraField("Rate", 99.8, "objects/ms");
        assertThat(task.getExtraField("Rate").name, is("Rate"));
        assertThat(task.getExtraField("Rate").val, is(99.8));
        assertThat(task.getExtraField("Rate").unit, is("objects/ms"));
        assertThat(task.getExtraField("Rate").getFormattedName(), is("Rate (objects/ms)"));
        assertThat(task.getExtraField("Rate").getFormattedValue(), is("99.8"));
    }

    @Test
    public void testTimeitForMillis() {
        LocalDateTime t1 = LocalDateTime.now();
        PerfTask task = new PerfTask("testTimeitForMillis", "Just a test", 10, 0, ctx -> { });
        task.timeitForMillis(3000L);
        assertThat(task.getName(), is("testTimeitForMillis"));
        assertThat(task.getDescription(), is("Just a test"));
        assertThat(task.getRunType(), is(PerfTask.RunType.TIMEIT_FOR_MILLIS));
        assertThat(task.getStop(), greaterThanOrEqualTo(task.getStart()));
        assertThat(task.getStop(), greaterThanOrEqualTo(t1));
        assertThat(task.getNumOfWarmupRuns(), is(10));
        assertThat(task.getNumOfRuns(), is(0));
        assertThat(task.getTotalRuns(), greaterThanOrEqualTo(1_000_000));
        assertThat(task.getTotalRunTime(), greaterThanOrEqualTo(3000L));
        assertThat(task.getMaxRunTime(), greaterThanOrEqualTo(0L));
        assertThat(task.getMinRunTime(), greaterThanOrEqualTo(0L));
        assertThat(task.getAvgRunTime(), greaterThanOrEqualTo(0.0));
        assertThat(task.getRate(), greaterThanOrEqualTo(1_000.0));

        Map<String, String> results = task.getTaskResults();
        assertThat(results, hasEntry("Name", "testTimeitForMillis"));
        assertThat(results, hasKey("Rate (runs/ms)"));
    }

    @Test
    public void testWarmupCount() {
        DCounter counter = new DCounter();
        PerfTask task = new PerfTask("testWarmupDoesNotCount", "Just a test", 10, 100, ctx -> {
            if (ctx.runType != PerfTask.RunType.WARMUP)
                counter.i ++;
            counter.i2 ++;
        });
        task.timeit();
        assertThat(counter.i, is(100));
        assertThat(counter.i2, is(110));
    }
}
