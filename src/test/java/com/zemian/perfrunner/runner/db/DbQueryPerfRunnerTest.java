package com.zemian.perfrunner.runner.db;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.runner.BasePerfRunnerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("db-runner")
public class DbQueryPerfRunnerTest extends BasePerfRunnerTest {

    @Autowired
    private DbQueryPerfRunner runner;

    @Override
    protected PerfRunner getRunner() {
        return runner;
    }

    @Test
    public void test() throws Exception {
        runner.testQuery("test", "SELECT 1 + 1");
    }
}
