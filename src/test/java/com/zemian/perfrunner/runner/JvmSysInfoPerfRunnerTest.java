package com.zemian.perfrunner.runner;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.runner.BasePerfRunnerTest;
import com.zemian.perfrunner.runner.JvmSysInfoPerfRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public class JvmSysInfoPerfRunnerTest extends BasePerfRunnerTest {

	@Autowired
	private JvmSysInfoPerfRunner runner;

	@Override
	protected PerfRunner getRunner() {
		return runner;
	}

	@Test
	public void testRun() throws Exception {
		runner.run();
	}

	@Test
	public void testSysInfo() throws Exception {
		Map<String, String> sysInfo = new TreeMap<>();
		Properties sysProps = System.getProperties();
		for (String name : sysProps.stringPropertyNames()) {
			sysInfo.put(name, sysProps.getProperty(name));
		}

		sysInfo.clear();
		sysInfo.putAll(System.getenv());
		for (Map.Entry<String, String> e : sysInfo.entrySet()) {
			System.out.println(e);
		}
	}
}