package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.runner.BasePerfRunnerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class JvmCollectionPerfRunnerTest extends BasePerfRunnerTest {

    @Autowired
    private JvmCollectionPerfRunner runner;

    @Override
    protected PerfRunner getRunner() {
        return runner;
    }

    @Test
    public void compareMap() throws Exception {
        runner.testColHashMap();
        runner.testColConcurrentHashMap();
        runner.testColSynchronizedHashMap();
    }
}