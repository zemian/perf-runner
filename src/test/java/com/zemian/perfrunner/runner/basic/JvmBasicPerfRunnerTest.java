package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.runner.BasePerfRunnerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class JvmBasicPerfRunnerTest extends BasePerfRunnerTest {

    @Autowired
    private JvmBasicPerfRunner runner;

    @Override
    protected PerfRunner getRunner() {
        return runner;
    }

    @Test
    public void testEmptyMethod() throws Exception {
        runner.testEmptyMethod();
    }
}
