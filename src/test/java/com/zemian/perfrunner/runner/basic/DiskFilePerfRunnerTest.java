package com.zemian.perfrunner.runner.basic;

import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.runner.BasePerfRunnerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DiskFilePerfRunnerTest extends BasePerfRunnerTest {

    @Autowired
    private DiskFilePerfRunner runner;

    @Override
    protected PerfRunner getRunner() {
        return runner;
    }

    @Test
    public void test() throws Exception {
        runner.run();
    }
}
