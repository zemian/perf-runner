package com.zemian.perfrunner.runner;

import com.zemian.perfrunner.AppMain;
import com.zemian.perfrunner.PerfRunner;
import com.zemian.perfrunner.RunnerSharedData;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppMain.class)
@ActiveProfiles({"test", "adoc-writer", "basic-runner"})
public abstract class BasePerfRunnerTest {

    protected RunnerSharedData runnerSharedData = new RunnerSharedData();

    protected abstract PerfRunner getRunner();

    @PostConstruct
    void init() throws Exception {
        getRunner().init(runnerSharedData);
    }

    @PreDestroy
    void destroy() throws Exception {
        getRunner().destroy();
    }
}
